<?php 

$screens = array();

foreach (range(1, 20) as $value) {
    $item['id'] = $value;
    $item['name'] = 'Screen prototype ' . $value;
    $item['image_path'] = ($value % 2 == 0) ? 'wireframe.jpg' : 'wireframe-2.png';
    $screens[] = $item;
}

?>
<div class="reference-inner">
    
    <div id="hook"></div>
    
    <form id="linkspot" method="post">
        <div class="control-group">
            <label class="control-label">Link to:</label>
            <div class="controls">
                <input type="hidden" id="screen" name="screen" value="">
                <span class="select" id="select">
                    <span id="text-value">Select a screen...</span> 
                    <i>&#9660;</i>
                </span>
                <div id="dropdown" class="dropdown">
                    <ul class="screens">
                        <li id="opt-external-url" class="option divider">External URL</li>
                        <?php foreach ($screens as $screen): ?>
                        <li data-id="<?php echo $screen['id'] ?>" 
                            data-thumb="<?php echo 'images/' . $screen['image_path'] ?>" 
                            class="option">
                            <?php echo $screen['name'] ?>                            
                        </li>
                        <?php endforeach ?>
                    </ul>
                </div>
                <div id="screen-thumbnail"></div>                
            </div>            
        </div>
        
        <div class="control-group" id="field-external-url">
            <input type="text" value="http://" name="external_url" id="external_url" class="input-block-level">
        </div>
        
        <div class="control-group">
            <label class="checkbox">                          
                <input type="checkbox" value="1" name="new_window">
                Open in new window  
            </label>            
        </div>
        
        <div class="control-group">
            <button class="btn btn-success">save</button>
        </div>

    </form>
    
</div>
    