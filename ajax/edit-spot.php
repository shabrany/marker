<?php 

$screens = array();

foreach (range(1, 30) as $value) {
    $item['id'] = $value;
    $item['name'] = 'Screen prototype ' . $value;
    $item['image_path'] = ($value % 2 == 0) ? 'wireframe.jpg' : 'wireframe-2.png';
    $screens[] = $item;
}

?>

<div class="reference-inner">
    
    <form id="linkspot" method="post">
        <!-- spot id -->
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
        <input type="hidden" name="update" value="1">
        <div class="control-group">
            
            <label class="form-label">Link to:</label>       
                        
            <div class="controls">
                <input type="hidden" id="screen" name="screen" value="<?php echo $_GET['screen'] ?>">
                <span class="select" id="select">
                    <span id="text-value">Screen prototype <?php echo $_GET['screen'] ?></span> 
                    <i>&#9660;</i>
                </span>
                <div id="dropdown" class="dropdown">
                    <ul class="screens">
                        <?php 
                        // Check if the spot has an external url. 
                        // If true, give the first list item the class active
                        // The code below is just an workaround for this prototype
                        $cls_active = ($_GET['screen'] == 0) ? 'active' : ''; 
                        ?>
                        <li id="opt-external-url" class="option divider <?php echo $cls_active ?>">External URL</li>
                        <?php foreach ($screens as $screen): ?>
                        <?php $selected = ($screen['id'] == $_GET['screen']) ? 'active' : ''; ?>
                        <li data-id="<?php echo $screen['id'] ?>" 
                            data-thumb="<?php echo 'images/' . $screen['image_path'] ?>" 
                            class="option <?php echo $selected ?>">
                            <?php echo $screen['name'] ?>                            
                        </li>
                        <?php endforeach ?>
                    </ul>
                </div>
                <div id="screen-thumbnail"></div>                
            </div>                       
            
        </div>
        <?php 
        // Check if the spot has an external url. 
        // if true, display the next field below        
        // The code below is just an workaround for this prototype
        $display = ($_GET['screen'] == 0) ? 'style="display: block"' : ''; 
        $external_url = 'http://google.nl' // must be dynamic
        ?>
        <div class="control-group" id="field-external-url" <?php echo $display ?>>
            <input type="text" value="http://" name="external_url" id="external_url" class="input-block-level">
        </div>
        
        <div class="control-group">
            <label class="checkbox">                          
                <input type="checkbox" value="1" name="new_window">
                Open in new window  
            </label>            
        </div>
        
        <div class="control-group">
            <button class="btn btn-success">save</button>
            <button id="delete" type="button">delete</button> 
        </div>

    </form>    
    
</div>
