<?php $notes = require 'notes.php'; ?>

<?php if (count($notes)): ?>

	<?php foreach ($notes as $key => $note): ?>

		<div class="note-wrapper" style="top: <?php echo $note['coord_y'] ?>px; left: <?php echo $note['coord_x'] ?>px;">
			
			<span class="note-pin">
				<span class="pin-inner" style="background: #5ec30d;"></span>
			</span>

			<div class="note-content">
				
				<?php foreach ($note['comments'] as $key => $comment): ?>
				<div class="comment">
					<span class="delete-comment fa fa-trash" id="comment-id-<?php echo $comment['id'] ?>">
						<span class="confirm">Delete?</span>
					</span>
					<p><?php echo $comment['message'] ?></p>
					<i>&#8212; <?php echo $comment['author'] ?></i>
				</div>
				<?php endforeach ?>
				
				<form method="post" action="save.php" class="new-comment">
					<input type="hidden" name="screen_id" value="1" />					
					<div class="comment-box">
						<textarea name="message" class="message"></textarea>
					</div>					
					<button type="submit">Post comment</button>
				</form>

			</div>
		</div>
		
	<?php endforeach ?>

<?php endif ?>