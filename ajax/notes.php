<?php

return $notes = array(
	array(
		'id' => 1,
		'coord_x' => 145,
		'coord_y' => 14,
		'comments' => array(
			array(
				'id' => 234,
				'author' => 'John Doe',
				'message' => 'Lorem ipsum escrito por un poeta muy sabio'
			),
			array(
				'id' => 235,
				'author' => 'Jessica Simpson',
				'message' => 'Hello world'
			),
			array(
				'id' => 249,
				'author' => 'John Doe',
				'message' => 'Dummy text over here'
			),
		)
	),
	array(
		'id' => 2,
		'coord_x' => 40,
		'coord_y' => 151,
		'comments' => array(
			array(
				'id' => 435,
				'author' => 'John Doe',
				'message' => 'Awesome plugin over here'
			),
		)
	),
	array(
		'id' => 3,
		'coord_x' => 296,
		'coord_y' => 225,
		'comments' => array(
			array(
				'id' => 567,
				'author' => 'John Doe',
				'message' => 'Awesome plugin over here'
			),
			array(
				'id' => 678,
				'author' => 'Jessica Simpson',
				'message' => 'No way!'
			),
		)
	),
);
