<html>
  <head>
    <title>Opentok Quick Start</title>
    <script src='http://static.opentok.com/webrtc/v2.2/js/opentok.min.js'></script>
    <script type="text/javascript">
      // Initialize API key, session, and token...
      // Think of a session as a room, and a token as the key to get in to the room
      // Sessions and tokens are generated on your server and passed down to the client
      var apiKey = "45159292";
      var sessionId = "2_MX40NTE1OTI5Mn5-MTQyNDM1MjY4NDI1Mn52VSthR3lZWnQ5VmNiaUprelQ3N0g4dmN-fg";
      var token = "T1==cGFydG5lcl9pZD00NTE1OTI5MiZzaWc9MTcxMmViOWY0ZDE3YTA4YmIwMGY4ODIyYTEzNTE1OTdmNGUwYzlkMDpyb2xlPXB1Ymxpc2hlciZzZXNzaW9uX2lkPTJfTVg0ME5URTFPVEk1TW41LU1UUXlORE0xTWpZNE5ESTFNbjUyVlN0aFIzbFpXblE1Vm1OaWFVcHJlbFEzTjBnNGRtTi1mZyZjcmVhdGVfdGltZT0xNDI0MzUyNzAwJm5vbmNlPTAuNjc3MjE3ODQ3MjMzNDI4NA==";

      // Initialize session, set up event listeners, and connect
      var session = OT.initSession(apiKey, sessionId);
 
//      session.on("streamCreated", function(event) {
//        session.subscribe(event.stream);
//      });
//     
      session.connect(token, function(error) {
//        var publisher = OT.initPublisher();
//        session.publish(publisher);
      });
      
      var connection1 = session.connectionId;
      
      session.signal(
        {
          to: connection1,
          data:"hello"
        },
        function(error) {
          if (error) {
            console.log("signal error ("
                         + error.code
                         + "): " + error.message);
          } else {
            console.log("signal sent.");
          }
        }
      );
    </script>
  </head>
  <body>
    <h1>Awesome video feed!</h1>
    <div id="myPublisherDiv"></div>
  </body>
</html>