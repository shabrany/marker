

<script type="text/javascript">

var screen_image = document.getElementById('screen_image');

var query_params = '?bounding_box=800&mode=fit';

var options = {

    // Required. Called when a user selects an item in the Chooser.
    success: function(files) {
        console.log(files[0]);
        var image_url = files[0].thumbnailLink.split('?')[0] + query_params;
        screen_image.src = image_url;
    },

    // Optional. Called when the user closes the dialog without selecting a file
    // and does not include any parameters.
    cancel: function() {

    },

    // Optional. "preview" (default) is a preview link to the document for sharing,
    // "direct" is an expiring link to download the contents of the file. For more
    // information about link types, see Link types below.
    linkType: 'preview',

    // Optional. A value of false (default) limits selection to a single file, while
    // true enables multiple file selection.
    multiselect: false,

    // Optional. This is a list of file extensions. If specified, the user will
    // only be able to select files with these extensions. You may also specify
    // file types, such as "video" or "images" in the list. For more information,
    // see File types below. By default, all extensions are allowed.
    extensions: ['images'],
};

var button = Dropbox.createChooseButton(options);
document.getElementById("dropbox-chooser").appendChild(button);

</script>
</body>
</html>