<!DOCTYPE html>
<html>
<head>
    <title>marker</title>
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css">
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="tpt4cd9880qy56c"></script>
</head>
<body>
    <br />
    <div class="toolbar" style="text-align: center;">
        <a class="btn btn-success" href="index.php">View</a>
        <a class="btn btn-success" href="note.php">Notes</a>
        <a class="btn btn-success" href="hotspot.php">Hotspots</a>
    </div>