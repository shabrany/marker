<?php include 'include/header.php'; ?>

<br />
<div id="main" class="m-scene">
    <div class="project_stage scene_element scene_element--fadein">
        <div id="screen_container" class="project-screen view">
            <img id="screen_image" src="images/wireframe.jpg" />
            <a href="index.php?pid=1&sid=4" class="spot" style="top: 218px; left: 3px; width: 115px; height: 31px;"></a>
            <a href="about.php?pid=1&sid=7" class="spot" style="top: 1px; left: 308px; width: 93px; height: 41px;"></a>
        </div>        
    </div>

    <div id="dropbox-chooser"></div>
</div>





<script type="text/javascript">
    
    (function ($) {

        var $spots, $container;

        $(document).ready(function () {

            $spots = $('.spot');
            $container = $('#screen_container');
            $spots.css('opacity', 0);
            // $spots.on('click', goToReference);
            highlightSpots();
            $container.on('click', function (e) {
                if (e.target.id === 'screen_image')
                    highlightSpots();
            });
        });

        function goToReference() {
            var screen_ref_id = $(this).data('target');
            var url = 'index.php?pid=1&sid=' + screen_ref_id;
            window.location.href = url;
        }

        function highlightSpots(e) {
            $spots.stop().animate({opacity: 0.6}, function () {
                $spots.stop().animate({opacity: 0});
            });
        }

    })(jQuery);





</script>

<?php include 'include/footer.php' ?>