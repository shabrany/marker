(function($) {

    var $screen_container = $('#screen_container');
    
    $(document).ready(function() {

        $screen_container.on('click', function(e) 
        {
            if(e.target.id == 'screen_image') {
                var coords = { x: e.offsetX, y: e.offsetY };         
                createNote($(this), coords);  
            }         
        });

        loadNotes($screen_container);  

        deleteComment();    
    });


    function createNote($parent, coords) 
    {
        removeEmptyNotes(); 

        var r = $screen_container.width() - coords.x;  
        
        var $note_wrapper = $('<div>', { class: 'note-wrapper'}).css({
            top: coords.y - 11,
            left: coords.x - 11
        });

        $.get('ajax/create-note.html', function(response)
        {
            $note_wrapper.html(response);    
            $note_wrapper.find('.note-content').fadeIn(100);    

            // Add mouserenter/mouseleave functionality
            addHandleMouseOver($note_wrapper);

            // Add submit functionality
            $form = $note_wrapper.find('form.new-comment-note');              
            postComment($form, true, coords);  
        });                     

        $parent.append($note_wrapper);
    }


    function addHandleMouseOver($note_wrapper) 
    {
        var z_index = 1000;

        $note_wrapper.on('mouseenter', function() {
            $(this).find('.note-content').addClass('active').stop().fadeIn(100);
            $(this).css({'z-index': z_index++});
        });
        
        $note_wrapper.on('mouseleave', function() {    
            var hasFocus = ($(this).find('textarea').is(':focus'));
            var hasComments = ($note_wrapper.find('.comment').length > 0);

            if (!hasFocus) {     
                $(this).children('.note-content').stop().fadeOut(100, function() {
                    $(this).css({'z-index': 0});   
                });                                    
            }

            if (!hasFocus && !hasComments) {
                $note_wrapper.remove();
            }     
                
            $(this).children('.note-content').removeClass('active');
        });
    }


    function postComment($form, create_note, coords) 
    { 
        $form.submit( function(e) {

            e.preventDefault();

            var $this_form = $(this);
            var data = $this_form.serializeArray();

            if ($this_form.find('textarea[name=message]').val().trim() == '')
                return false;

            if (create_note) {
                data.push(
                    { name: 'coordX', value: coords.x }, 
                    { name: 'coordY', value: coords.y }, 
                    { name: 'new_note', value: 1 }
                );
            }

            $.post('ajax/save.php', data, function (response) {
                
                $this_form.find('textarea[name=message]').val('');
                $this_form.before(appendNewComment(response));

                // Add delete comment functionality
                deleteComment();

            }, 'json');                
        });                      
    }


    function appendNewComment(data) 
    {
        var $div_comment = $('<div>', {class: 'comment'}),
            $message = $('<p>').text(data.message),
            $author = $('<i>').html('&#8212; ' + data.author),
            $delete_comment = $('<span>', { class: 'delete-comment fa fa-trash', id: 'comment-id-' + data.id });            

        $delete_comment.html('<span class="confirm">Delete?</span>');

        $div_comment.append($delete_comment).append($message).append($author);

        return $div_comment;
    }


    function removeEmptyNotes() 
    {   
        $.each($('.note-wrapper'), function(index, element) 
        {
            var $note_wrapper = $(element);
            var comments = $note_wrapper.find('.comment');
            var $note_content = $note_wrapper.find('.note-content');

            if (comments.length == 0) {
                $note_wrapper.remove();
            }                     
                
            if (!$note_content.is('.active')) {                    
                $note_content.not('.active').stop().fadeOut(100);
                $note_wrapper.css({'z-index': 0});
            }                       
        });
    }


    function loadNotes ($parent) 
    {   
        $.get('ajax/show-notes.php', function(response) {
            
            $parent.append(response);  

            var $note_wrapper = $('.note-wrapper');
            
            // Add mouserenter/mouseleave functionality
            addHandleMouseOver($note_wrapper);   

            closeOpenNotes(); 

            // Add submit functionality
            postComment($('form.new-comment'), false, null);

            // Add delete comment functionality
            deleteComment();

        }, 'html');   
    }


    function closeOpenNotes() 
    {
        $('textarea.message').on('focus', function() {
            removeEmptyNotes();
        });
    }

    function deleteComment() 
    {
        $('.delete-comment').click(function() {

            var comment_id = this.id.replace( /^\D+/g, '');
            var $parent = $(this).parent('.comment');
            var $confirm = $(this).children('.confirm');

            $confirm.stop().animate({ width: 44, left: -38 }, 150);

            $parent.on('mouseleave', function() {               
                $confirm.css({ width: 0, left: 0, display: 'none' });
            });

            $confirm.on('click', function() {

                // delete commen using ajax
                // if the last comment is removed, remove also the note 
                // from the database
                
                // remove comment on succes
                $parent.remove();

            });
        });
    }
          



} )(jQuery);
     
