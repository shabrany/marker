

(function ($) 
{
    var spot = {};
    var $container = $('#screen_container');
    var $referenceWrapper = $('<div>', { class: 'reference-wrapper'});
    var $spot_area;
    var ui_helper;

    $(document).ready(function() {
        
        $container.selectable({
            cancel: '.spot, .reference-wrapper',
            selecting: function(event, ui) {    
                ui_helper = $('.ui-selectable-helper')[0];     
                removeEmptySpots();
                $referenceWrapper.remove();
            },
            stop: function(event, ui) {

                spot.startX = (ui_helper.offsetLeft - this.offsetLeft);
                spot.startY = (ui_helper.offsetTop - this.offsetTop);
                spot.width = ui_helper.offsetWidth;
                spot.height = ui_helper.offsetHeight;

                $spot_area = $('<div>', { class: 'spot' });
                $spot_area.css({
                    top: spot.startY,
                    left: spot.startX,
                    width: spot.width,
                    height: spot.height
                });

                if (spot.width > 30 && spot.height > 25) {
                    $container.append($spot_area);
                    createSpotReference();
                }                   
            }
        });

        addSpotClickEvent();
    });
        

    function createSpotReference() {
        var gutter = 10;  

        $.get('ajax/create-spot.php', function(response) {
            $referenceWrapper.html(response);
            $('form#linkspot').submit(saveSpot);
            $('span#select').click(selectScreen);
        });

        $referenceWrapper.css({
            top: spot.startY,
            left: (spot.startX + spot.width + gutter),
            display: 'none'
        });

        $container.append($referenceWrapper.fadeIn(100));
    }


    function saveSpot(e) {                
        e.preventDefault();
        var this_form = $(this);
        var data = this_form.serializeArray();

        // Only on update
        if (this_form.find('input[name=update]').length == 0) {
            console.log('creating...');
            data.push(
                { name: 'coord_x', value: spot.startX },
                { name: 'coord_y', value: spot.startY },
                { name: 'width', value: spot.width },
                { name: 'height', value: spot.height }
            );
        } else {
            console.log('updating...');            
        }

        var $posting = $.post('ajax/save-spot.php', data, function(response) {     
            console.log(response);
            $spot_area.data('screen', response.data.screen);
            $spot_area.data('id', response.data.id);      
            if (!response.data.update)             
                addSpotClickEvent($spot_area);                 
        }, 'json');

        $posting.done( function(response) {
            $referenceWrapper.fadeOut(100);
        });

        $posting.fail(function() {
            console.log('Spot could not be saved..');
        });
    }


    function removeEmptySpots() {
        $.each($('.spot.ui-selectee'), function(index, element) {
            if (!$(element).data('id')) {
                $(element).remove();
            }
        });
    }


    function addSpotClickEvent($element) {
        var $spot_area = $element || $('.spot');
        $spot_area.click(editSpotReference);
    }


    function editSpotReference(e) {
        $spot_area = $(this);
        var gutter = 10;
        var data = { id: $(this).data('id'), screen: $(this).data('screen') };
        
        var $get = $.get('ajax/edit-spot.php', data);                    
        
        $get.done( function(response) {            
            $referenceWrapper.html(response);
            $('form#linkspot').submit(saveSpot).find('a#delete').click(deleteSpot);
            $('button#delete').click(deleteSpot);
            $('span#select').unbind('click').bind('click', selectScreen);
        });

        $referenceWrapper.css({
            top: this.offsetTop,
            left: (this.offsetLeft + this.offsetWidth + gutter),
            display: 'none'
        });

        $container.append($referenceWrapper.fadeIn(100));
    } 
    
    
    function selectScreen(){
        var $select_box = $(this);
        var $dropdown = $(this).next('div#dropdown');
        var $screen_thumb = $('#screen-thumbnail');
        var $opts = $dropdown.find('li.option');
        var $input_screen = $('input#screen');                           
        
        $opts.hover(function(e){
            var thumb_url = $(this).data('thumb');     
            if (thumb_url) {
                $screen_thumb.css('top', (this.offsetTop + 60) - $dropdown[0].scrollTop);
                $screen_thumb.html('<img src="'+thumb_url+'">');        
                $screen_thumb.show();
            } else {
                $screen_thumb.hide();
            }
        });
        
        $opts.unbind('click').bind('click', function() {
            $input_screen.val($(this).data('id'));
            $select_box.children('#text-value').text($(this).text());
            $dropdown.stop().slideUp(100);
            $opts.removeClass('active');
            $(this).addClass('active');
            if (this.id == 'opt-external-url') {
                $('#field-external-url').show();
            } else {
                $('#field-external-url').hide();
            }
        });
        
        $dropdown.mouseleave(function() { 
            $screen_thumb.hide(); 
        });   
        
        $dropdown.stop().slideToggle(100);  
    }


    function deleteSpot(e) {
        
       // $.post('ajax/delete-spot.php', { id: $spot.data('id') }, function(response) {}, 'json');
        
        $spot_area.remove();
        $referenceWrapper.fadeOut(100, function() { $(this).remove(); });
        
    }


    function displayScreen() {

    }


})(jQuery);

